package com.systempermissions;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

/**
 * Created by Hari on 04/04/16.
 */
public class BaseActivity extends AppCompatActivity {
    public static final String TAG = BaseActivity.class.getSimpleName();
    public static String[] PERMISSIONS_IMAGE_CHOOSER = {
            Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE
    };
    public static String[] PERMISSIONS_TAKE_PICTURE = {
            Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE
    };
    protected static String[] PERMISSIONS_VIDEO_CHOOSER = {
            Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE
    };
    protected static String[] PERMISSIONS_PICK_PHOTO = {
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    public static final int REQUEST_TAKE_PICTURE = 1;
    public static final int REQUEST_PICK_PHOTO = 2;
    public static final int REQUEST_IMAGE_CHOOSER = 3;
    public static final int REQUEST_VIDEO_CHOOSER = 4;
    public static final int REQUEST_PERMISSION_TAKE_PICTURE = 11;
    public static final int REQUEST_PERMISSION_PICK_PHOTO = 12;
    public static final int REQUEST_PERMISSION_IMAGE_CHOOSER = 13;
    public static final int REQUEST_PERMISSION_VIDEO_CHOOSER = 14;
    private ProgressDialog dialog;

    private String mCurrentPhotoPath;

    protected void showProgressDialog(String message) {
        dialog = new ProgressDialog(this);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.show();
    }

    protected void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
                .show();
    }

    protected void showProgressDialog() {
        showProgressDialog("Please Wait..");
    }

    protected void dismissProgressDialog() {
        try {
            dialog.dismiss();
        } catch (Exception e) {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        dismissProgressDialog();
        switch (requestCode) {
            case REQUEST_PERMISSION_PICK_PHOTO: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchPickPhotoIntent();
                } else {
                    showToast("You cannot send images unless you allow humOS access to the photos on your device...");
                }
                return;
            }
            case REQUEST_PERMISSION_TAKE_PICTURE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent();
                } else {
                    showToast("You cannot take and send images unless you allow humOS access to the cam on your device...");
                }
                return;
            }
            case REQUEST_PERMISSION_IMAGE_CHOOSER: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchImageChooserIntent();
                } else {
                    showToast("You cannot take and send images unless you allow humOS access to the camera and files on your device...");
                }
                return;
            }
            case REQUEST_PERMISSION_VIDEO_CHOOSER: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchVideoChooserIntent();
                } else {
                    showToast("You cannot take and send images unless you allow humOS access to the camera and files on your device...");
                }
                return;
            }
        }
    }

    protected void dispatchTakePictureIntent() {
        int camPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (camPermission != PackageManager.PERMISSION_GRANTED || storePermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_TAKE_PICTURE,
                    REQUEST_PERMISSION_TAKE_PICTURE);
        } else {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile = createImageFile();
                if (photoFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE);
                }
            } else {
                showToast("You can't take pictures...");
            }
        }
    }

    public void dispatchPickPhotoIntent() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "Permission not granted, granting now...");
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_PICK_PHOTO,
                    REQUEST_PERMISSION_PICK_PHOTO
            );
        } else {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, REQUEST_PICK_PHOTO);
        }
    }

    public void dispatchImageChooserIntent() {
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_IMAGE_CHOOSER,
                    REQUEST_PERMISSION_IMAGE_CHOOSER);
        } else {
            Intent pickImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickImageIntent.putExtra("pick", true);

            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra("pick", false);

            String pickTitle = "Choose Action";
            Intent chooserIntent = Intent.createChooser(pickImageIntent, pickTitle);
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takePictureIntent});

            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile = createImageFile();
                if (photoFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    startActivityForResult(chooserIntent, REQUEST_IMAGE_CHOOSER);
                } else {
                    startActivityForResult(pickImageIntent, REQUEST_IMAGE_CHOOSER);
                }
            } else {
                startActivityForResult(pickImageIntent, REQUEST_IMAGE_CHOOSER);
            }
        }
    }

    private File createImageFile() {
        String imageFileName = Constants.ME + System.currentTimeMillis();
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/" + Constants.ME);
        if (!storageDir.exists())
            storageDir.mkdir();
        File image = null;
        try {
            image = File.createTempFile(imageFileName, ".jpg", storageDir);
        } catch (IOException e) {
            Log.e(TAG, "Error in createImageFile: " + e.getMessage());
        }
        setCurrentPhotoPath(image.getAbsolutePath());
        return image;
    }

    public void setCurrentPhotoPath(String mCurrentPhotoPath) {
        this.mCurrentPhotoPath = mCurrentPhotoPath;
    }

    public String getCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    public void dispatchVideoChooserIntent() {
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_VIDEO_CHOOSER,
                    REQUEST_PERMISSION_VIDEO_CHOOSER);
        } else {
            Intent pickVideoIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);

            String pickTitle = "Choose Action";
            Intent chooserIntent = Intent.createChooser(pickVideoIntent, pickTitle);
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takeVideoIntent});

            if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(chooserIntent, REQUEST_VIDEO_CHOOSER);
            } else {
                startActivityForResult(pickVideoIntent, REQUEST_VIDEO_CHOOSER);
            }
        }
    }

}